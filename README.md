yii2-oauth2-server
==================

A wrapper for implementing an OAuth2 Server(https://gitee.com/cuifox/yii2-oauth2-server.git)

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```shell script
php composer.phar require --prefer-dist cuifox/yii2-oauth2-server "*"
```

or add

```json
"cuifox/yii2-oauth2-server": "^1.0"
```

to the require section of your composer.json.


To use this extension,  simply add the following code in your application configuration:

```php
'modules' => [
    'oauth2' => [
        //'class' => 'filsh\yii2\oauth2server\Module',
        'class' => 'CuiFox\oauth2server\Module',
        'tokenParamName' => 'access_token', // 访问令牌参数名称（可选）
        'tokenAccessLifetime' => 3600, // 访问令牌有效期（可选）
        'storageMap' => [
            'user_credentials' => 'app\models\User', // 用于验证用户凭据的存储器（根据你的应用需求替换）
            'user_claims' => 'app\models\User', // 用于验证用户凭据的存储器（根据你的应用需求替换）
        ],
        'grantTypes' => [
            'authorization_code' => [
                'class' => 'OAuth2\GrantType\AuthorizationCode',
            ],
            'user_credentials' => [
                'class' => 'OAuth2\GrantType\UserCredentials',
            ],
            'client_credentials' => [
                'class' => 'OAuth2\GrantType\ClientCredentials',
            ],
            'refresh_token' => [
                'class' => 'OAuth2\GrantType\RefreshToken',
                'config' => [
                    'always_issue_new_refresh_token' => true,
                ],
            ],
            'jwt_bearer' => [
                'class' => 'OAuth2\GrantType\JwtBearer',
                'audience' => 'http://yii-abc.com',
            ],
            // 此处需要options中的OpenID Connect
            /*'authorization_code' => [
                'class' => 'OAuth2\OpenID\GrantType\AuthorizationCode',
            ],*/
        ],
        'options' => [
            'allow_implicit' => true,
            // configure the server for OpenID Connect
            /*'use_openid_connect' => true,
            'issuer' => 'yii-abc.com',*/
        ],
        //'useJwtToken' => true,
    ],
],
```

```common\models\User``` - user model implementing an interface ```\OAuth2\Storage\UserCredentialsInterface```, so the oauth2 credentials data stored in user table

You can pass additional OAuth2 Server Options by setting `options` property on the module. Some of them are implemented as standalone properties on the module: `tokenParamName`, `tokenAccessLifetime`, `useJwtToken`. For a full list of the supported options go to the - [source code](https://github.com/bshaffer/oauth2-server-php/blob/5a0c8000d4763b276919e2106f54eddda6bc50fa/src/OAuth2/Server.php#L162).


this migration creates the oauth2 database scheme and insert test user credentials ```testclient:testpass``` for ```http://fake/```